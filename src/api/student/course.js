import request from '@/utils/request'

// 查询查看课程列表
export function listCourse(query) {
  return request({
    url: '/student/course/list',
    method: 'get',
    params: query
  })
}

// 查询查看课程详细
export function getCourse(id) {
  return request({
    url: '/student/course/' + id,
    method: 'get'
  })
}

// 新增查看课程
export function addCourse(data) {
  return request({
    url: '/student/course',
    method: 'post',
    data: data
  })
}

// 修改查看课程
export function updateCourse(data) {
  return request({
    url: '/student/course',
    method: 'put',
    data: data
  })
}

// 删除查看课程
export function delCourse(id) {
  return request({
    url: '/student/course/' + id,
    method: 'delete'
  })
}
