import request from '@/utils/request'

// 查询课程类别设置列表
export function listCategory(query) {
  return request({
    url: '/information/category/list',
    method: 'get',
    params: query
  })
}

// 查询课程类别设置详细
export function getCategory(id) {
  return request({
    url: '/information/category/' + id,
    method: 'get'
  })
}

// 新增课程类别设置
export function addCategory(data) {
  return request({
    url: '/information/category',
    method: 'post',
    data: data
  })
}

// 修改课程类别设置
export function updateCategory(data) {
  return request({
    url: '/information/category',
    method: 'put',
    data: data
  })
}

// 删除课程类别设置
export function delCategory(id) {
  return request({
    url: '/information/category/' + id,
    method: 'delete'
  })
}
