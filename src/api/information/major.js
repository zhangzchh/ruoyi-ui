import request from '@/utils/request'

// 查询专业设置列表
export function listMajor(query) {
  return request({
    url: '/information/major/list',
    method: 'get',
    params: query
  })
}

// 查询专业设置详细
export function getMajor(majorId) {
  return request({
    url: '/information/major/' + majorId,
    method: 'get'
  })
}

// 新增专业设置
export function addMajor(data) {
  return request({
    url: '/information/major',
    method: 'post',
    data: data
  })
}

// 修改专业设置
export function updateMajor(data) {
  return request({
    url: '/information/major',
    method: 'put',
    data: data
  })
}

// 删除专业设置
export function delMajor(majorId) {
  return request({
    url: '/information/major/' + majorId,
    method: 'delete'
  })
}
