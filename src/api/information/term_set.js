import request from '@/utils/request'

// 查询学期设置列表
export function listTerm_set(query) {
  return request({
    url: '/information/term_set/list',
    method: 'get',
    params: query
  })
}

// 查询学期设置详细
export function getTerm_set(id) {
  return request({
    url: '/information/term_set/' + id,
    method: 'get'
  })
}

// 新增学期设置
export function addTerm_set(data) {
  return request({
    url: '/information/term_set',
    method: 'post',
    data: data
  })
}

// 修改学期设置
export function updateTerm_set(data) {
  return request({
    url: '/information/term_set',
    method: 'put',
    data: data
  })
}

// 删除学期设置
export function delTerm_set(id) {
  return request({
    url: '/information/term_set/' + id,
    method: 'delete'
  })
}
