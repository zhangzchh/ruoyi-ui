import request from '@/utils/request'

// 查询学生设置列表
export function listStudent(query) {
  return request({
    url: '/information/student/list',
    method: 'get',
    params: query
  })
}

// 查询学生设置详细
export function getStudent(id) {
  return request({
    url: '/information/student/' + id,
    method: 'get'
  })
}

// 新增学生设置
export function addStudent(data) {
  return request({
    url: '/information/student',
    method: 'post',
    data: data
  })
}

// 修改学生设置
export function updateStudent(data) {
  return request({
    url: '/information/student',
    method: 'put',
    data: data
  })
}

// 删除学生设置
export function delStudent(id) {
  return request({
    url: '/information/student/' + id,
    method: 'delete'
  })
}
