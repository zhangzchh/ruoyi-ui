import request from '@/utils/request'

// 查询教师设置列表
export function listTea_information(query) {
  return request({
    url: '/information/tea_information/list',
    method: 'get',
    params: query
  })
}

// 查询教师设置详细
export function getTea_information(id) {
  return request({
    url: '/information/tea_information/' + id,
    method: 'get'
  })
}

// 新增教师设置
export function addTea_information(data) {
  return request({
    url: '/information/tea_information',
    method: 'post',
    data: data
  })
}

// 修改教师设置
export function updateTea_information(data) {
  return request({
    url: '/information/tea_information',
    method: 'put',
    data: data
  })
}

// 删除教师设置
export function delTea_information(id) {
  return request({
    url: '/information/tea_information/' + id,
    method: 'delete'
  })
}
