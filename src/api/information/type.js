import request from '@/utils/request'

// 查询教室类型列表
export function listType(query) {
  return request({
    url: '/information/type/list',
    method: 'get',
    params: query
  })
}

// 查询教室类型详细
export function getType(id) {
  return request({
    url: '/information/type/' + id,
    method: 'get'
  })
}

// 新增教室类型
export function addType(data) {
  return request({
    url: '/information/type',
    method: 'post',
    data: data
  })
}

// 修改教室类型
export function updateType(data) {
  return request({
    url: '/information/type',
    method: 'put',
    data: data
  })
}

// 删除教室类型
export function delType(id) {
  return request({
    url: '/information/type/' + id,
    method: 'delete'
  })
}
