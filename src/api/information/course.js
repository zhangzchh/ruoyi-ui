import request from '@/utils/request'

// 查询课程设置列表
export function listCourse(query) {
  return request({
    url: '/information/course/list',
    method: 'get',
    params: query
  })
}

// 查询课程设置详细
export function getCourse(courseId) {
  return request({
    url: '/information/course/' + courseId,
    method: 'get'
  })
}

// 新增课程设置
export function addCourse(data) {
  return request({
    url: '/information/course',
    method: 'post',
    data: data
  })
}

// 修改课程设置
export function updateCourse(data) {
  return request({
    url: '/information/course',
    method: 'put',
    data: data
  })
}

// 删除课程设置
export function delCourse(courseId) {
  return request({
    url: '/information/course/' + courseId,
    method: 'delete'
  })
}
