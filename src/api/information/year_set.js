import request from '@/utils/request'

// 查询学年设置列表
export function listYear_set(query) {
  return request({
    url: '/information/year_set/list',
    method: 'get',
    params: query
  })
}

// 查询学年设置详细
export function getYear_set(id) {
  return request({
    url: '/information/year_set/' + id,
    method: 'get'
  })
}

// 新增学年设置
export function addYear_set(data) {
  return request({
    url: '/information/year_set',
    method: 'post',
    data: data
  })
}

// 修改学年设置
export function updateYear_set(data) {
  return request({
    url: '/information/year_set',
    method: 'put',
    data: data
  })
}

// 删除学年设置
export function delYear_set(id) {
  return request({
    url: '/information/year_set/' + id,
    method: 'delete'
  })
}
