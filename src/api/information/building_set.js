import request from '@/utils/request'

// 查询楼栋设置列表
export function listBuilding_set(query) {
  return request({
    url: '/information/building_set/list',
    method: 'get',
    params: query
  })
}

// 查询楼栋设置详细
export function getBuilding_set(id) {
  return request({
    url: '/information/building_set/' + id,
    method: 'get'
  })
}

// 新增楼栋设置
export function addBuilding_set(data) {
  return request({
    url: '/information/building_set',
    method: 'post',
    data: data
  })
}

// 修改楼栋设置
export function updateBuilding_set(data) {
  return request({
    url: '/information/building_set',
    method: 'put',
    data: data
  })
}

// 删除楼栋设置
export function delBuilding_set(id) {
  return request({
    url: '/information/building_set/' + id,
    method: 'delete'
  })
}
