import request from '@/utils/request'

// 查询教室设置列表
export function listClassroom(query) {
  return request({
    url: '/information/classroom/list',
    method: 'get',
    params: query
  })
}

// 查询教室设置详细
export function getClassroom(id) {
  return request({
    url: '/information/classroom/' + id,
    method: 'get'
  })
}

// 新增教室设置
export function addClassroom(data) {
  return request({
    url: '/information/classroom',
    method: 'post',
    data: data
  })
}

// 修改教室设置
export function updateClassroom(data) {
  return request({
    url: '/information/classroom',
    method: 'put',
    data: data
  })
}

// 删除教室设置
export function delClassroom(id) {
  return request({
    url: '/information/classroom/' + id,
    method: 'delete'
  })
}
