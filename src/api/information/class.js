import request from '@/utils/request'

// 查询班级设置列表
export function listClass(query) {
  return request({
    url: '/information/class/list',
    method: 'get',
    params: query
  })
}

// 查询班级设置详细
export function getClass(classId) {
  return request({
    url: '/information/class/' + classId,
    method: 'get'
  })
}

// 新增班级设置
export function addClass(data) {
  return request({
    url: '/information/class',
    method: 'post',
    data: data
  })
}

// 修改班级设置
export function updateClass(data) {
  return request({
    url: '/information/class',
    method: 'put',
    data: data
  })
}

// 删除班级设置
export function delClass(classId) {
  return request({
    url: '/information/class/' + classId,
    method: 'delete'
  })
}
