import request from '@/utils/request'

// 查询学院设置列表
export function listCollage(query) {
  return request({
    url: '/information/collage/list',
    method: 'get',
    params: query
  })
}

// 查询学院设置详细
export function getCollage(id) {
  return request({
    url: '/information/collage/' + id,
    method: 'get'
  })
}

// 新增学院设置
export function addCollage(data) {
  return request({
    url: '/information/collage',
    method: 'post',
    data: data
  })
}

// 修改学院设置
export function updateCollage(data) {
  return request({
    url: '/information/collage',
    method: 'put',
    data: data
  })
}

// 删除学院设置
export function delCollage(id) {
  return request({
    url: '/information/collage/' + id,
    method: 'delete'
  })
}
